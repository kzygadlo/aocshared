﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOCShared
{    
    public interface IAoc<out T1, out T2>
    {
        T1 GetResultPart1();
        T2 GetResultPart2();
        void SolveAndPrintResults();
    }
}
