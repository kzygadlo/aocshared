﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace AOCShared
{
    public static class ArrayExtension
    {
        public static void FillWithValue(this char[,] input, char charToFill)
        {
            var x = input.GetLength(0);
            var y = input.GetLength(1);

            for( var i = 0; i < x; i++)
            {
                for( var j = 0; j < y; j++)
                {
                    input[i, j] = charToFill;
                }
            }
        }

        public static int[,] FillWithValue(this int[,] input, int valueToFill)
        {
            var x = input.GetLength(0);
            var y = input.GetLength(1);

            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    input[i, j] = valueToFill;
                }
            }

            return input;
        }

        public static void PrintToConsole(int[,] input)
        {
            for ( var y = 0; y < input.GetLength(0); y++)
            {
                for  (var x = 0; x < input.GetLength(1); x++)
                {
                    Console.Write(input[y, x]);
                }
                Console.WriteLine();
            }
        }
    }
}
