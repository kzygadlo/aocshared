﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOCShared
{
    public static class Globals
    {
        public const string InputFilesPath = "Assets/";
        public const string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const char Up = 'U';
        public const char Down = 'D';
        public const char Left = 'L';
        public const char Right = 'R';
    }
}
