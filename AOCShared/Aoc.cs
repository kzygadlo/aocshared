﻿namespace AOCShared
{
    public abstract class Aoc<TInput, TResult1, TResult2> : IAoc<TResult1, TResult2> where TInput : new()
    {
        protected TInput Input;
        private readonly string _fileName;
        protected Aoc(string fileName)
        {
            _fileName = fileName;
            Input = GetDataFromTheFile();
        }
        private TInput GetDataFromTheFile()
        {
            try
            {
                var path = Path.Combine(Globals.InputFilesPath, $"{_fileName}.txt");
                if (!File.Exists(path))
                {
                    throw new FileNotFoundException($"File {_fileName}.txt not found.");
                }

                var inputFile = File.ReadAllLines(path);
                #pragma warning disable CS8600 
                #pragma warning disable CS8603 
                return (TInput)Activator.CreateInstance(typeof(TInput), new object[] { inputFile });
                #pragma warning restore CS8600 
                #pragma warning restore CS8603
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error while loading data from file: {ex.Message}");
                throw;
            }
        }

        public abstract TResult1 GetResultPart1();
        public abstract TResult2 GetResultPart2();

        public void SolveAndPrintResults()
        {
            Console.WriteLine($"{_fileName} | Part 1 result: {GetResultPart1()}");
            Console.WriteLine($"{_fileName} | Part 2 result: {GetResultPart2()}");
            Console.WriteLine();
        }
    }
}
